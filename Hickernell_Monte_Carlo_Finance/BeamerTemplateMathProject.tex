%Template file for a presentation using Latex and beamer
%You will need to install Latex, the beamer package, and some of these other packages on your own.
\documentclass[11pt,compress]{beamer} %slides and notes
\usepackage{amsmath,epstopdf,datetime,array,alltt,graphicx}
\usepackage[autolinebreaks]{mcodefred}
\usepackage[author-year]{amsrefs}
\usepackage{textcomp} %helps to compile MATLAB code in mcode or lstlisting

\usetheme{FredIIT}
\logo{\includegraphics[width=1cm]{MengerIITRedGray.pdf}}

%Here we make some definitions
\DeclareMathOperator{\sign}{sign}

\title[Quadratic Equation]{Numerically Solving the Quadratic Equation}
\author[hickernell@iit.edu]{Fred J. Hickernell}
\institute{Room 208, Bldg E1, Department of Applied Mathematics \\
Phone: 312 567 8983 \\
Email: \href{mailto:hickernell@iit.edu}{\url{hickernell@iit.edu}} \\
Web: \href{http://mypages.iit.edu/~hickernell}{\url{mypages.iit.edu/~hickernell}}} 
\date[{Revised \currenttime, \mdyyyydate \today}]{Revised \settimeformat{ampmtime} \currenttime, \today}

\begin{document}

\frame{\titlepage}

\section{The Quadratic Equation}
\begin{frame}\frametitle{The Quadratic Equation}

\begin{tabular}{m{5.5cm}>{\centering}m{6cm}}
A quadratic equation contains terms that are constant, linear in the unknown variable, and quadratic in the unknown variable.  The general form of a quadratic equation for a real variable $x$ is 
\[
a x^2 + bx + c = 0.
\]
An example is plotted on the right.

\bigskip

The quadratic equation arises in precalculus \cite{Young14a}*{Sect.\ 0.2} or even earlier.
&
\includegraphics[width=6cm]{ProgramsImages/quadraticequationplot.eps}
\end{tabular}
\end{frame}

\begin{frame}\frametitle{The Quadratic Formula}

\begin{tabular}{m{5.5cm}>{\centering}m{6cm}}
\begin{minipage}{5.5cm}
\[
a x^2 + bx + c = 0
\]

\bigskip

\begin{theorem}[Quadratic Formula] The quadratic equation has two solutions, $x_{\pm}$, given by
\[
x_{\pm} = \frac{-b \pm \sqrt{b^2-4ac}}{2 a}.
\]
\end{theorem}
\end{minipage}
&
\includegraphics[width=6cm]{ProgramsImages/quadraticequationplot.eps}
\end{tabular}
\end{frame}

\begin{frame}\frametitle{Proof of the Quadratic Formula}
\vspace{-2ex}
\begin{proof} 
Beginning with the quadratic equation we first complete the square:
\begin{align*}
0 & = a x^2 + bx + c = a \left (x^2 + \frac{b}{a} x + \frac{b^2}{4 a^2} \right) - \frac{b^2}{4 a} + c \\
&= a \left (x + \frac{b}{2a} \right)^2 - \frac{b^2 - 4 ac}{4 a}, \\[-1ex]
\intertext{and then solve for $x$:} \\[-6ex]
\left (x + \frac{b}{2a} \right)^2 & = \frac{b^2 - 4 ac}{4 a^2} \\
x + \frac{b}{2a} & = \pm \frac{\sqrt{b^2 - 4 ac}}{2a} \\
x & = \frac{-b \pm \sqrt{b^2 - 4 ac}}{2a}  \qquad \qed
\end{align*}
\end{proof}
\end{frame}

\begin{frame}[fragile]\frametitle{MATLAB Code for the Quadratic Formula}
The quadratic formula, which gives the solution for the quadratic equation can be written in MATLAB as shown in the following example:

\begin{alltt}
>> a=1, b=-3, c=2
a =
     1
b =
    -3
c =
     2
>> x=(-b+[-1 1]*sqrt(b^2-4*a*c))/(2*a)
x =
     1     2
\end{alltt}

\end{frame}

\section{Overflow and Underflow Problems}
\begin{frame}[fragile]\frametitle{Overflow and Underflow}
The natural expression for the quadratic formula is susceptible to \alert{round-off errors} \cite{Moler04a}*{Problem 1.38}.  One variety is \alert{overflow}, 
\begin{alltt}
>> a=1e200; b=-3e200; c=2e200;
>> x=(-b+[-1 1]*sqrt(b^2-4*a*c))/(2*a)
x =
   NaN   NaN
\end{alltt}
and \alert{underflow},
\begin{alltt}
>> a=1e-200; b=-3e-200; c=2e-200;
>> x=(-b+[-1 1]*sqrt(b^2-4*a*c))/(2*a)
x =
     1.500000000000000e+00     1.500000000000000e+00
\end{alltt}
In both cases the actual answers should be $1$ and $2$, but they are not.
\end{frame}

\begin{frame}[fragile]\frametitle{Scale to Solve Overflow and Underflow}
The solution to overflow and underflow is to scale the coefficients of the quadratic equation so that the largest one is one in magnitude.
\begin{alltt}
>> a=1e200; b=-3e200; c=2e200;
>> scale=abs(max([a b c])); a=a/scale; b=b/scale; c=c/scale;
>> x=(-b+[-1 1]*sqrt(b^2-4*a*c))/(2*a)
x =
     1     2
>> a=1e-200; b=-3e-200; c=2e-200;
>> scale=abs(max([a b c])); a=a/scale; b=b/scale; c=c/scale;
>> x=(-b+[-1 1]*sqrt(b^2-4*a*c))/(2*a)
x =
     1     2
\end{alltt}
The overflow and underflow problems are now gone.
\end{frame}

\section{Cancellation Error Problems}
\begin{frame}[fragile]\frametitle{Cancellation Error}
The natural expression for the quadratic formula is also susceptible to \alert{cancellation error} when $4|ac|$ is much smaller than $|b|$, 
\begin{alltt}
>> a=1; b=-3e20; c=2;
>> x=(-b+[-1 1]*sqrt(b^2-4*a*c))/(2*a)
x =
                         0     3.000000000000000e+20
\end{alltt}
Although one root is $3$ (to 15 significant digits), the other root cannot be $0$.  But in this case $-b$ and $\sqrt{b^2-4ac}$ are numerically the same, which give a $0$ answer.  Scaling the coefficients does not help.
\end{frame}

\begin{frame}[fragile]\frametitle{Avoid Cancellation Error by a Rewrite}
The solution to the cancellation error problem is to rewrite the expression for the quadratic formula to avoid subtracting two nearly equal quantities.  First we rewrite the quadratic formula slightly as
\[
x_{\pm}  = \frac{-b \pm \sign(b)\sqrt{b^2 - 4 ac}} {2a}, \qquad \text{where }\sign(b) := \begin{cases} -1, & b < 0, \\ 1 & b \ge 0.
\end{cases}
\]
Now it is clear that $x_-$ has no cancellation problems, but $x_+$ might, so we write
\begin{align*}
x_{+}  &= \frac{-b + \sign(b)\sqrt{b^2 - 4 ac}} {2a} \times \frac{-b - \sign(b)\sqrt{b^2 - 4 ac}}{-b - \sign(b)\sqrt{b^2 - 4 ac}} \\
&= \frac{2c}{-b - \sign(b)\sqrt{b^2 - 4 ac}}
\end{align*}
There is no serious loss of precision in this last expression.
\end{frame}

\begin{frame}[fragile]\frametitle{Avoid Cancellation Error by a Rewrite}
We now retry the previous example using the rewrite:
\begin{alltt}
>> a=1; b=-3e20; c=2;
>> term=-b+sign(b)*sqrt(b^2-4*a*c);
>> term=-b-sign(b)*sqrt(b^2-4*a*c);
>> x=[term/(2*a) (2*c)/term]
x =
     3.000000000000000e+20     6.666666666666667e-21
\end{alltt}
We get both roots to nearly 15 significant digit accuracy.
\end{frame}

\section{Final Algorithm}
\begin{frame}\frametitle{\texttt{qeq.m}}
Putting both ideas together we end up with the following MATLAB function to evaluate the quadratic formula in a way that avoids round-off error problems.
\lstinputlisting[linerange=1-11]{ProgramsImages/qeq.m}
\end{frame}

\begin{frame}\frametitle{\texttt{qeq.m} cont'd}
\lstinputlisting[firstline=12]{ProgramsImages/qeq.m}
\end{frame}

\begin{frame}[fragile]\frametitle{\texttt{qeq.m} Examples}
Now we try all of our examples again.  The answers are correct to nearly $15$ significant digits in all cases.
\begin{alltt}
>> x=qeq(1,-3,2)
x =
     9.999999999999999e-01     2.000000000000000e+00
>> x=qeq(1e200,-3e200,2e200)
x =
     9.999999999999999e-01     2.000000000000000e+00
>> x=qeq(1e-200,-3e-200,2e-200)
x =
     9.999999999999999e-01     2.000000000000000e+00
>> x=qeq(1,-3e20,2)
x =
     6.666666666666667e-21     3.000000000000000e+20
\end{alltt}    
\end{frame}

\begin{frame}[fragile]\frametitle{Summary}
\begin{itemize}
\item The quadratic equation, and its solution expressed by the quadratic formula are fundamental ideas

\item The numerical evaluation of the quadratic formula as it stands is prone to serious \alert{round-off} error problems for some choices of the coefficients.

\item \alert{Scaling} the coefficients avoids unnecessary overflow or underflow.

\item \alert{Rewriting} one of the solutions avoids unnecessary cancellation error.

\end{itemize}  
\end{frame}


\part{References}
\begin{frame}\frametitle{References}
\begin{bibdiv}
\begin{biblist}
    \bib{Moler04a}{book}{
        title={Numerical Computing with {MATLAB}},
        author={Moler, Cleve},
        date={2004},
        publisher={SIAM},
        address={Philadelphia}
}
    \bib{Young14a}{book}{
        title={Precalculus},
        author={Young, Cynthia Y.},
        date={2014},
        publisher={John Wiley \& Sons},
        address={New York},
        edition={Second Edition}
}
\end{biblist}
\end{bibdiv}
\end{frame}
\end{document}
